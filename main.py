"""
Person Information Analysis

This script defines a list of people with their names, last names, and ages. It performs various analyses on the list and displays the results.

Data:
- Personnes (list): A list of people with their information.

Analysis Operations:
1. If there is a person named "Michel" or a person aged 50 or older, display the message: "On a dans la liste quelqu'un qui adore le camping !"
2. Display the information of all people who love camping in the format: "C'est formidable [Names] adorent le camping !"
3. If people have the same number of letters in their first name as in their last name, display a message in the format: "C'est formidable [Name] a [LetterCount] lettre dans son nom et prénoms."
4. Display unique information for people with the same first name, if any, in the format: "(xxx, xxx, xxx) et (xxx, xxx, xxx) et (xxx, xxx, xxx) et ... ont des prénoms identiques."

Usage:
Run the script to see the results of each analysis operation.

Example:
python script_name.py
"""


import random

Personnes =\
    [["Pascal" , "ROSE", "43 ans"],
    ["Mickaël", "FLEUR", "29 ans"],
    ["Henri", "TULIPE", "35 ans"],
    ["Michel", "FRAMBOISE", "35 ans"],
    ["Arthur", "PETALE", "35 ans"],
    ["Michel", "POLLEN", "50 ans"],
    ["Michel", "FRAMBOISE", "42 ans"]]




random.shuffle(Personnes)

#1) Si on a dans la liste une personne qui s'appelle Michel ou une personne qui a 50 ans ou plus afficher une seule fois le message suivant :
for i in range(0,len(Personnes)):
    if Personnes[i][0] == "Michel":
        print("On a dans la liste quelqu'un qui adore le camping !")
        break

#2) On veut maintenant afficher les informations de toutes les personnes qui adorent le camping sous la forme :
Names = ""
for i in range(0,len(Personnes)):
    if Personnes[i][0] == "Michel":
        Names += ", " + Personnes[i][0] + " " + Personnes[i][1]
print("C'est formidable " + Names + " adore le camping !")



#3) Si des personnes ont le même nombre de lettres dans leur prénom que dans leur nom, afficher un message sous la forme :
for i in range(0,len(Personnes)):
    if len(Personnes[i][0]) == len(Personnes[i][1]):
        print("C'est formidable " + Personnes[i][0] + " " + Personnes[i][1] + " a " + str(len(Personnes[i][0])) + " lettre dans son nom et prénoms")


#4) Afficher sans doublon les informations des personnes qui ont le même prénom s'il y en a sous la forme :
Names = []
previousName = ""
for personne in Personnes:
    for personneCheck in Personnes:
        if personne[0] == personneCheck[0] and personneCheck not in Names:
            previousName = personne[0]
            Names += [personneCheck]
            print(Names)



print("(xxx, xxx, xxx) et (xxx, xxx, xxx) et (xxx, xxx, xxx) et ... ont des prénoms identiques")



